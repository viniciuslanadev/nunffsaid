import React, { useContext, useState, useEffect } from "react";

// Components
import { Header, Container } from "./components/Layout";
import { MessagesContainer, Message } from "./components/Messages";
import Snackbar from "./components/Snackbar";

// Contexts
import {
  MessagesContextProvider,
  MessagesContext,
} from "./contexts/MessageContext";

// Utils
import { MessageWithId } from "./utils/useMessages";

function filterMessagesByPriority(messages: MessageWithId[]) {
  return {
    error: messages.filter((message) => message.priority === 0).reverse() || [],
    warning:
      messages.filter((message) => message.priority === 1).reverse() || [],
    info: messages.filter((message) => message.priority === 2).reverse() || [],
  };
}

const App: React.FC<{}> = () => {
  const { messages, onClearMessage } = useContext(MessagesContext);

  const [snackbarMessage, setSnackbarMessage] = useState<
    MessageWithId | undefined
  >(undefined);
  const [showSnackBar, setShowSnackbar] = useState<boolean>(false);

  const messagesByPriority = filterMessagesByPriority(messages);
  const hasPriorityMessages = messagesByPriority.error.length > 0;

  const messageColumns = [
    {
      title: "Error type 1",
      messages: messagesByPriority.error || [],
      priority: "Error",
    },
    {
      title: "Warning type 2",
      messages: messagesByPriority.warning || [],
      priority: "Warning",
    },
    {
      title: "Info type 3",
      messages: messagesByPriority.info || [],
      priority: "Info",
    },
  ];

  useEffect(() => {
    const incomingMessage = messagesByPriority.error[0];
    if (hasPriorityMessages && incomingMessage.id !== snackbarMessage?.id) {
      setShowSnackbar(true);
      setSnackbarMessage(incomingMessage);
    }
  }, [
    messagesByPriority.error,
    setShowSnackbar,
    hasPriorityMessages,
    snackbarMessage?.id,
  ]);

  return (
    <>
      <Header />
      {showSnackBar && hasPriorityMessages && (
        <Snackbar
          message={snackbarMessage?.message}
          open={showSnackBar}
          onClose={() => setShowSnackbar(false)}
        />
      )}

      <Container>
        {messageColumns.map((column, key) => (
          <MessagesContainer
            title={column.title}
            subtTitle={`count ${column.messages.length}`}
            key={key}
          >
            {column.messages.map((msg) => (
              <Message
                key={msg.message}
                onClear={onClearMessage}
                content={msg.message}
                priority={column.priority}
                id={msg.id}
              />
            ))}
          </MessagesContainer>
        ))}
      </Container>
    </>
  );
};

const AppWrapper = () => (
  <MessagesContextProvider>
    <App />
  </MessagesContextProvider>
);

export default AppWrapper;
