import IconButton from "@material-ui/core/IconButton";
import MUISnackbar, { SnackbarProps } from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";

function Snackbar(props: SnackbarProps) {
  const { className, message, onClose, open, ...other } = props;

  return (
    <MUISnackbar
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={open}
      onClose={onClose}
      autoHideDuration={1000}
    >
      <SnackbarContent
        aria-describedby="client-snackbar"
        message={<span id="client-snackbar">{message}</span>}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={(e) => onClose && onClose(e, "")}
          >
            X
          </IconButton>,
        ]}
        {...other}
      />
    </MUISnackbar>
  );
}

export default Snackbar;
