import styled from "styled-components";
import Card from "@material-ui/core/Card";

// TODO Improvement move it to a .styles.tsx file
const Message = styled(Card)`
  display: flex;
  flex-direction: column;
  padding: 1em;
  margin-bottom: 0.5em;
`;

const ClearButton = styled.button`
  background: transparent;
  border: none;
  width: 60px;
  align-self: flex-end;
`;

export type Priority = "Error" | "Warning" | "Info";

export interface MessageProps {
  id: string;
  content: string;
  priority: Priority | string;
  onClear: (v: string) => void;
}

enum MessageBackgroundColors {
  Error = "#F56236",
  Warning = "#FCE788",
  Info = "#88FCA3",
}

const MessageImpl = ({ id, content, priority, onClear }: MessageProps) => {
  return (
    <Message
      data-testid={`message__${priority}__${id}`}
      style={{
        // We also could have it being handled by styled components
        backgroundColor: MessageBackgroundColors[priority as Priority],
      }}
    >
      {content}
      <ClearButton type="button" onClick={() => onClear(id)}>
        Clear
      </ClearButton>
    </Message>
  );
};

export default MessageImpl;
