import React from "react";
import styled from "styled-components";

// TODO Improvement move it to a .styles.tsx file
const MessageContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export interface MessagesContainerProps {
  title: string;
  subtTitle: string;
  children: React.ReactNode;
}

const MessagesContainerImpl = ({
  title,
  subtTitle,
  children,
}: MessagesContainerProps) => {
  return (
    <MessageContainer>
      <section>
        <header>
          <h2>{title}</h2>
          <h3>{subtTitle}</h3>
        </header>
        <div>{children}</div>
      </section>
    </MessageContainer>
  );
};

export default MessagesContainerImpl;
