import React from "react";
import { render } from "@testing-library/react";
import MessagesContainer, {
  MessagesContainerProps,
} from "../MessagesContainer";
import faker from "faker";

const defaultProps: MessagesContainerProps = {
  subtTitle: "",
  title: "",
  children: <></>,
};

function renderComponent(props: MessagesContainerProps = defaultProps) {
  return render(<MessagesContainer {...props} />);
}

test("renders MessagesContainer", () => {
  const component = renderComponent();
  expect(component).toMatchSnapshot();
});

test("renders MessagesContainer with custom properties", () => {
  const message = faker.lorem.sentence();
  const CustomComponent = () => <div>{message}</div>;

  const customProps: MessagesContainerProps = {
    title: faker.lorem.sentence(),
    subtTitle: faker.lorem.sentence(),
    children: <CustomComponent />,
  };

  const { getByText } = renderComponent(customProps);
  expect(getByText(customProps.title)).toBeVisible();
  expect(getByText(customProps.subtTitle)).toBeVisible();
  expect(getByText(message)).toBeVisible();
});
