import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Message, { MessageProps } from "../Message";
import faker from "faker";
import * as uuid from "uuid";

const defaultProps: MessageProps = {
  id: uuid.v4(),
  content: faker.lorem.sentence(),
  onClear: jest.fn(),
  type: "info",
};

function renderComponent(props: MessageProps = defaultProps) {
  return render(<Message {...props} />);
}

test("renders Message type info", () => {
  const component = renderComponent();
  expect(component).toMatchSnapshot();
  expect(
    component.getByTestId(`message__info__${defaultProps.id}`)
  ).toBeVisible();
});

test("renders Message type error", () => {
  const component = renderComponent({ ...defaultProps, type: "error" });
  expect(component).toMatchSnapshot();
  expect(
    component.getByTestId(`message__error__${defaultProps.id}`)
  ).toBeVisible();
});

test("renders Message type warning", () => {
  const component = renderComponent({ ...defaultProps, type: "warning" });
  expect(component).toMatchSnapshot();
  expect(
    component.getByTestId(`message__warning__${defaultProps.id}`)
  ).toBeVisible();
});

test("renders Message with content", () => {
  const messageContent = faker.lorem.sentence();
  const component = renderComponent({
    ...defaultProps,
    content: messageContent,
  });

  expect(component.getByText(messageContent)).toBeVisible();
});

test("emit event onClear", () => {
  const component = renderComponent({
    ...defaultProps,
  });

  fireEvent.click(component.getByText("Clear"));
  expect(defaultProps.onClear).toHaveBeenCalledTimes(1);
});
