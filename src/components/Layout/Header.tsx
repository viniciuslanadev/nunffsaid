import React, { useContext, useState } from "react";
import styled from "styled-components";
import Button from "@material-ui/core/Button";

// Contexts
import { MessagesContext } from "../../contexts/MessageContext";

// TODO Improvement move it to a .styles.tsx file

// Not ideal, I'm using the terrible but helpfull css property '!imporant' here,
// but the better approach here would
// be follow the custumization guide as we can see here
// https://v3.mui.com/demos/buttons/#api
const ActionButton = styled(Button)`
  text-transform: uppercase;
  background-color: #88fca3 !important;
  font-weight: bold !important;
  margin: 0.2em !important;
`;

const ActionPanel = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 3em;
`;

const Header = styled.header`
  margin: 0 0.5em 1em 0.5em;
  border-bottom: 1px solid #cccccc;
`;

const HeaderImpl: React.FC = () => {
  const [showPlayButton, setShowPlayButton] = useState<boolean>(false);
  const { onStopMessages, onPlayMessages, onClearMessages } =
    useContext(MessagesContext);

  return (
    <>
      <Header>
        <h1>nunffsaid.com Coding Challenge</h1>
      </Header>
      <ActionPanel>
        {!showPlayButton && (
          <ActionButton
            variant="contained"
            onClick={() => {
              onStopMessages();
              setShowPlayButton(true);
            }}
          >
            Stop
          </ActionButton>
        )}

        {showPlayButton && (
          <ActionButton
            variant="contained"
            onClick={() => {
              onPlayMessages();
              setShowPlayButton(false);
            }}
          >
            Play
          </ActionButton>
        )}

        <ActionButton variant="contained" onClick={onClearMessages}>
          Clear
        </ActionButton>
      </ActionPanel>
    </>
  );
};

export default HeaderImpl;
