import styled from "styled-components";

export default styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 2em;
  max-width: 1280px;
  margin: auto;
`;
