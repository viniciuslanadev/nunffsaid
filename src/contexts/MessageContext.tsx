import React, { createContext } from "react";
import { useMessages, MessageWithId } from "../utils/useMessages";

interface MessagesContextProps {
  messages: MessageWithId[];
  onClearMessages: () => void;
  onClearMessage: (v: string) => void;
  onStopMessages: () => void;
  onPlayMessages: () => void;
}

export const initialContextValues = {
  messages: [],
  onClearMessages: () => {},
  onClearMessage: (v: string) => {},
  onStopMessages: () => {},
  onPlayMessages: () => {},
};

export const MessagesContext =
  createContext<MessagesContextProps>(initialContextValues);

export const MessagesContextProvider: React.FC = ({ children }) => {
  const {
    messages,
    handleRequestMessages,
    handleClearMessages,
    handleClearMessageById,
  } = useMessages();

  return (
    <MessagesContext.Provider
      value={{
        messages,
        onPlayMessages: () => handleRequestMessages(),
        onStopMessages: () => handleRequestMessages(true),
        onClearMessages: handleClearMessages,
        onClearMessage: handleClearMessageById,
      }}
    >
      {children}
    </MessagesContext.Provider>
  );
};
