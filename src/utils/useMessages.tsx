import { useState, useEffect } from "react";
import generateMessage, { Message } from "../Api";
import * as uuid from "uuid";

export interface MessageWithId extends Message {
  id: string;
}

interface HookUseMessages {
  messages: MessageWithId[];
  handleRequestMessages: (v?: boolean) => void;
  handleClearMessages: (v?: boolean) => void;
  handleClearMessageById: (id: string) => void;
}

export const useMessages = (): HookUseMessages => {
  const [messages, setMessages] = useState<MessageWithId[]>([]);
  const [messagesFeed, setMessagesFeed] = useState<MessageWithId[]>([]);
  const [stopMessages, setStopMessages] = useState<boolean>(false);

  const handleRequestMessages = (stop: boolean = false) => {
    setStopMessages(stop);
  };

  const handleClearMessages = () => {
    setMessages([]);
    setMessagesFeed([]);
  };

  const handleClearMessageById = (id: string) => {
    const stopMessagesStatus = stopMessages;
    setStopMessages(true);

    const newMessagesFeed = messagesFeed.filter((msg) => msg.id !== id);
    const newMessages = messages.filter((msg) => msg.id !== id);

    setMessagesFeed(newMessagesFeed);
    setMessages(newMessages);
    setStopMessages(stopMessagesStatus);
  };

  useEffect(() => {
    const cleanUp = generateMessage((message: Message) => {
      // It will still running even when the user set messages to stop
      // This way we are able to show all the requested messages while
      // the user has stopped it
      setMessagesFeed((oldMessages) => [
        ...oldMessages,
        { id: uuid.v4(), ...message },
      ]);
    });
    return cleanUp;
  }, [setMessages]);

  useEffect(() => {
    if (!stopMessages) setMessages(messagesFeed);
  }, [stopMessages, messagesFeed]);

  return {
    messages,
    handleRequestMessages,
    handleClearMessages,
    handleClearMessageById,
  };
};
